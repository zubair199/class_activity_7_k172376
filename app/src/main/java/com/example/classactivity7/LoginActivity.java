package com.example.classactivity7;

import android.app.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.content.SharedPreferences;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.classactivity7.HomeActivity;
import com.example.classactivity7.R;

public class LoginActivity extends AppCompatActivity {

    EditText name, pass;
    CheckBox ch;
    SharedPreferences sp;
    Button login;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        name = (EditText) findViewById(R.id.username);

        pass = (EditText) findViewById(R.idpackage com.example.classactivity7;

import android.app.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.content.SharedPreferences;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.classactivity7.HomeActivity;
import com.example.classactivity7.R;

public class LoginActivity extends AppCompatActivity {

    EditText name, pass;
    CheckBox ch;
    SharedPreferences sp;
    Button login;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        name = (EditText) findViewById(R.id.username);

        pass = (EditText) findViewById(R.id.password);

        ch = (CheckBox) findViewById(R.id.checkBoxRememberMe);

        login = (Button) findViewById((R.id.login));

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            }
        });


        sp = (SharedPreferences) getSharedPreferences("MySharedPref",MODE_PRIVATE);

        SharedPreferences.Editor edit =sp.edit();

        if(getSharedPreferences("MySharedPref",MODE_PRIVATE)==null)
        {
            ch.setActivated(false);
        }
        else
        {
            ch.setActivated(true);
        }

        ch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean checked=((CheckBox) ch).isChecked();

                if(checked)
                {
                    edit.putString("username",name.getText().toString());
                    edit.putString("password",pass.getText().toString());

                    boolean commit=edit.commit();
                }
                else
                {
                    edit.clear();
                }

            }
        });

    }
}